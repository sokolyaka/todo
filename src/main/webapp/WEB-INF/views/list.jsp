<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>ToDo list</title>

    <style>
        a {
            text-decoration: none;
            color:black;
            font-weight: bold;
        }

        body {
            text-align: center;
        }

        table {
            margin: auto;
            padding: 10px;
        }

        tr:first-child {
            font-weight: bold;
            background-color: #C6C9C4;
            padding: 3px;
        }

        .delete_btn {
            color: red;
        }

        .td_edit {
            text-align: center;
        }

        .td_delete {
            text-align: center;
        }
    </style>

    <script type="text/javascript"
            src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var checkboxes = $("input[type=checkbox]");
            checkboxes.each(function (i, elem) {
                if (elem.value == "true") {
                    elem.setAttribute("checked", "checked");
                }
            })
        });
    </script>
</head>


<body>
<h2>ToDo list</h2>
<div>Filters</div>
<button><a href="<c:url value='/list/1' />">Done</a></button>
<button><a href="<c:url value='/list/0' />">Not done</a></button>
<button><a href="<c:url value='/list' />">All</a></button>
<div>
    <table>
        <tr>
            <td>Task</td>
            <td>Done</td>
            <td>Edit</td>
            <td>Delete</td>
        </tr>
        <c:forEach items="${todoList}" var="ToDo">
            <tr>
                <td>${ToDo.task}</td>
                <td><input type="checkbox" value="${ToDo.done}" disabled/></td>
                <td class="td_edit">
                    <button>
                        <a class="update_btn" href="<c:url value='/edit-${ToDo.id}-ToDo' />"> ... </a></button>
                </td>
                <td class="td_delete">
                    <button>
                        <a class="delete_btn" href="<c:url value='/delete-${ToDo.id}-ToDo' />"> X </a></button>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<br/>
<button><a href="<c:url value='/new' />">Add New Task</a></button>

</body>
</html>