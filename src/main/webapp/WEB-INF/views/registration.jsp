<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Add new task Form</title>

    <style>

        .error {
            color: #ff0000;
        }

        body {
            text-align: center;
        }
        table {
            margin: auto;
        }
    </style>

</head>

<body>

<h2>Add new task form</h2>

<form:form method="POST" modelAttribute="ToDo">
    <form:input type="hidden" path="id" id="id"/>
    <table>
        <tr>
            <td><label for="task">Task: </label></td>
            <td><form:input path="task" id="task" required="required"/></td>
            <td><form:errors path="task" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="done">done: </label></td>
            <td><form:checkbox path="done" id="done"/></td>
            <td><form:errors path="done" cssClass="error"/></td>
        </tr>

        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
<br/>
<br/>
Go back to <a href="<c:url value='/list' />">List of All Tasks</a>
</body>
</html>