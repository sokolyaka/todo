package com.todolist.springmvc.controller;

import com.todolist.springmvc.model.ToDo;
import com.todolist.springmvc.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/")
public class AppController {

	@Autowired
	ToDoService service;
	
	@Autowired
	MessageSource messageSource;

	@RequestMapping(value = { "/", "/list"}, method = RequestMethod.GET)
	public String listToDo(ModelMap model) {

		List<ToDo> todoList = service.findAllToDo();
		model.addAttribute("todoList", todoList);
		return "list";
	}
	@RequestMapping(value = {"/list/{done}"}, method = RequestMethod.GET)
	public String sortedListToDO(ModelMap model, @PathVariable Boolean done) {

		List<ToDo> todoList = service.findAllToDoByStatus(done);
		model.addAttribute("todoList", todoList);
		return "list";
	}


	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newToDo(ModelMap model) {
		ToDo toDo = new ToDo();
		model.addAttribute("ToDo", toDo);
		model.addAttribute("edit", false);
		return "registration";
	}


	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String saveToDo(@Valid ToDo toDo, BindingResult result,
							   ModelMap model) {

		if (result.hasErrors()) {
			return "registration";
		}

		service.saveToDo(toDo);

		model.addAttribute("success", "ToDo: " + toDo.getTask() + " added successfully");
		return "success";
	}


	@RequestMapping(value = { "/edit-{id}-ToDo" }, method = RequestMethod.GET)
	public String editToDo(@PathVariable Integer id, ModelMap model) {
		ToDo toDo = service.findById(id);
		model.addAttribute("ToDo", toDo);
		model.addAttribute("edit", true);
		return "registration";
	}
	
	@RequestMapping(value = { "/edit-{id}-ToDo" }, method = RequestMethod.POST)
	public String updateToDo(@Valid ToDo toDo, BindingResult result,
								 ModelMap model, @PathVariable Integer id) {

		if (result.hasErrors()) {
			return "registration";
		}

		service.updateToDo(toDo);
		model.addAttribute("success", "ToDo: " + toDo.getTask() + " updated successfully");
		return "success";
	}

	
	@RequestMapping(value = { "/delete-{id}-ToDo" }, method = RequestMethod.GET)
	public String deleteToDo(@PathVariable Integer id) {
		service.deleteToDoById(id);
		return "redirect:/list";
	}

}
