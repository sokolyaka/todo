package com.todolist.springmvc.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "ToDo")
public class ToDo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 3, max = 255)
    @Column(name = "TASK", nullable = false)
    private String task;

    @Column(name = "DONE", nullable = false)
    private Boolean done;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDo ToDo = (ToDo) o;

        if (id != ToDo.id) return false;
        if (task != null ? !task.equals(ToDo.task) : ToDo.task != null) return false;
        return done != null ? done.equals(ToDo.done) : ToDo.done == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (task != null ? task.hashCode() : 0);
        result = 31 * result + (done != null ? done.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ToDo{" +
                "id=" + id +
                ", task='" + task + '\'' +
                ", done=" + done +
                '}';
    }
}
