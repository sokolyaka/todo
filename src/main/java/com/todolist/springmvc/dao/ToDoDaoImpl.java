package com.todolist.springmvc.dao;

import com.todolist.springmvc.model.ToDo;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("toDoDao")
public class ToDoDaoImpl extends AbstractDao<Integer, ToDo> implements ToDoDao {

    public ToDo findById(int id) {
        return getByKey(id);
    }

    public void saveToDo(ToDo ToDo) {
        persist(ToDo);
    }

    public void deleteToDoById(int id) {
        Query query = getSession().createSQLQuery("delete from TODO where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<ToDo> findAllToDos() {
        Criteria criteria = createEntityCriteria();
        return (List<ToDo>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<ToDo> findAllToDoByStatus(Boolean done) {
        return (List<ToDo>) createEntityCriteria().add(Restrictions.eq("done", done)).list();
    }


}
