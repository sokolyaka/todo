package com.todolist.springmvc.dao;

import com.todolist.springmvc.model.ToDo;

import java.util.List;

public interface ToDoDao {

	ToDo findById(int id);

	void saveToDo(ToDo ToDo);
	
	void deleteToDoById(int id);
	
	List<ToDo> findAllToDos();

	List<ToDo> findAllToDoByStatus(Boolean done);

}
