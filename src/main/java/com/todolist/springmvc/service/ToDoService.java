package com.todolist.springmvc.service;

import com.todolist.springmvc.model.ToDo;

import java.util.List;

public interface ToDoService {

	ToDo findById(int id);
	
	void saveToDo(ToDo ToDo);
	
	void updateToDo(ToDo ToDo);
	
	void deleteToDoById(int id);

	List<ToDo> findAllToDo();

	List<ToDo> findAllToDoByStatus(Boolean done);

}
