package com.todolist.springmvc.service;

import com.todolist.springmvc.dao.ToDoDao;
import com.todolist.springmvc.model.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("todoService")
@Transactional
public class ToDoServiceImpl implements ToDoService {

    @Autowired
    private ToDoDao dao;

    public ToDo findById(int id) {
        return dao.findById(id);
    }

    public void saveToDo(ToDo ToDo) {
        dao.saveToDo(ToDo);
    }


    public void updateToDo(ToDo toDo) {
        ToDo entity = dao.findById(toDo.getId());
        if (entity != null) {
            entity.setDone(toDo.getDone());
            entity.setTask(toDo.getTask());
        }
    }

    public void deleteToDoById(int id) {
        dao.deleteToDoById(id);
    }

    public List<ToDo> findAllToDo() {
        return dao.findAllToDos();
    }

    public List<ToDo> findAllToDoByStatus(Boolean done) {
        return dao.findAllToDoByStatus(done);
    }

}
